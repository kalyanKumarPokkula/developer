import * as dotenv from 'dotenv'

dotenv.config();

const PORT = process.env.PORT
const DB_PASS = process.env.DB_PASS

export{
    PORT ,
    DB_PASS 
}