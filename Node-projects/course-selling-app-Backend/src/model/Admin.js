import mongoose from "mongoose";
import bcrypt from "bcrypt";
import { ADMIN_SALT } from "../config/config.js";

const adminSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      default: "",
    },
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unqiue: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      required: true,
      default: "Admin",
    },
  },
  { timestamps: true }
);

adminSchema.pre("save", function (next) {
  this.username = `${this.firstname} ${this.lastname}`;
  next();
});

adminSchema.pre("save", function (next) {
  let hashPassword = bcrypt.hashSync(this.password, ADMIN_SALT);
  this.password = hashPassword;
  next();
});

const Admin = mongoose.model("Admin", adminSchema);

export default Admin;
