import AdminRepository from "../repository/Admin-Reposiory.js";
import { generate_Admin_Jwt } from "../utils/Generate-Jwt.js";

class AdminService {
  constructor() {
    this.adminRepository = new AdminRepository();
  }

  async signUp(data) {
    try {
      let admin = await this.adminRepository.create(data);
      let token = generate_Admin_Jwt(admin);
      return token;
    } catch (error) {
      console.log("Something went wrong in ADMIN SERVICE");
      throw error;
    }
  }

  async signIn(data) {
    try {
      let admin = await this.adminRepository.getByEmail(email);
      if (!admin) {
        throw Error("Admin does't exists");
      }
    } catch (error) {
      console.log("Something went wrong in ADMIN SERVICE");
      throw error;
    }
  }
}

export default AdminService;
