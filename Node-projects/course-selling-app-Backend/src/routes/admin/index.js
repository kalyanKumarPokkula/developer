import express from "express";
import authenticateJwt from "../../middlewares/authenticateJwt.js";
import Admin from "../../model/Admin.js";

const router = express.Router();

router.post("/signup", async (req, res) => {
  try {
    console.log(req.body);
    let response = await Admin.create(req.body);
    // let token = generatejwt(response);
    return res.status(201).json({
      message: "Admin created successfully",
      success: true,
      err: {},
      token: response,
    });
  } catch (error) {
    console.log(error);
    return res.status(403).json({
      message: "Something went wrong",
      success: false,
      err: error,
    });
  }
});

router.post("/login", async (req, res) => {
  try {
    let filter = {
      username: req.headers.username,
      password: req.headers.password,
    };
    let response = await Admin.findOne(filter);
    let token = generatejwt(response);
    return res.status(201).json({
      message: "Logged in successfully",
      success: true,
      err: {},
      token: token,
    });
  } catch (error) {
    console.log(error);
    return res.status(403).json({
      message: "Something went wrong",
      success: false,
      err: error,
    });
  }
});

router.post("/courses", authenticateJwt, async (req, res) => {
  try {
    let response = await Course.create(req.body);
    return res.status(201).json({
      message: "Course created successfully",
      success: true,
      courseId: response.id,
    });
  } catch (error) {
    console.log(error);
    return res.status(403).json({
      message: "Soemthing went wrong",
      success: false,
      err: error,
    });
  }
});

router.put("/courses/:courseId", authenticateJwt, async (req, res) => {
  try {
    let courseId = req.params.courseId;
    let response = await Course.findByIdAndUpdate(courseId, req.body);
    return res.status(200).json({
      message: "Course updated successfully",
      success: true,
    });
  } catch (error) {
    console.log(error);
    return res.status(403).json({
      message: "Something went wrong",
      success: false,
      err: error,
    });
  }
});

router.get("/courses", authenticateJwt, async (req, res) => {
  try {
    let response = await Course.find({});
    return res.status(200).json({
      courses: response,
      success: true,
    });
  } catch (error) {
    return res.status(403).json({
      message: "Something went wrong",
      success: false,
      err: error,
    });
  }
});

export default router;
