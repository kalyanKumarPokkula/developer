import "./Register.css";
import Button from "../UI/Button/Button";
import { NavLink } from "react-router-dom";

const Register = () => {
  return (
    <form>
      <div className="title">
        <label>Register</label>
      </div>
      <div className="field">
        <label>Name</label>
        <input type="text" />
      </div>
      <div className="field">
        <label>Email</label>
        <input type="text" />
      </div>
      <div className="field">
        <label>Password</label>
        <input type="text" />
      </div>
      <Button>Sign up</Button>
      <div className="bottom">
        <p>Already a user? </p>
        <NavLink to="/login">Login</NavLink>
      </div>
    </form>
  );
};

export default Register;
