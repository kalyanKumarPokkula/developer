import { useState } from "react";
import "./App.css";

function App() {
  return (
    <div className="container-categories">
      <div className="container-category">
        <div className="bg-img">
          <img
            src="https://my-demo-bucket-ak.s3.ap-south-1.amazonaws.com/4k-minimalist-mountains-9oitratl6gd996za.jpg "
            alt=""
          />
        </div>
        <div className="category-body-container">
          <h2>hats</h2>
        </div>
      </div>
      <div className="container-category">
        <div className="bg-img">
          <img
            src="https://my-demo-bucket-ak.s3.ap-south-1.amazonaws.com/4k-minimalist-mountains-9oitratl6gd996za.jpg "
            alt=""
          />
        </div>
        <div className="category-body-container">
          <h2>hats</h2>
        </div>
      </div>
      <div className="container-category">
        <div className="bg-img">
          <img
            src="https://my-demo-bucket-ak.s3.ap-south-1.amazonaws.com/4k-minimalist-mountains-9oitratl6gd996za.jpg "
            alt=""
          />
        </div>
        <div className="category-body-container">
          <h2>hats</h2>
        </div>
      </div>
      <div className="container-category">
        <div className="bg-img">
          <img
            src="https://my-demo-bucket-ak.s3.ap-south-1.amazonaws.com/4k-minimalist-mountains-9oitratl6gd996za.jpg "
            alt=""
          />
        </div>
        <div className="category-body-container">
          <h2>hats</h2>
        </div>
      </div>
      <div className="container-category">
        <div className="bg-img">
          <img
            src="https://my-demo-bucket-ak.s3.ap-south-1.amazonaws.com/4k-minimalist-mountains-9oitratl6gd996za.jpg "
            alt=""
          />
        </div>
        <div className="category-body-container">
          <h2>hats</h2>
        </div>
      </div>
    </div>
  );
}

export default App;
