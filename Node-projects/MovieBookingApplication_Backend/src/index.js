import express from "express";
import bodyParser from "body-parser";

import {PORT} from './config/server-config.js'
import { connect } from "./config/db-config.js";
import Movie from "./models/movie.js";


const SetupServer = async () => {

    const app = express();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended : true}));

    app.get('/' , (req ,res) => {
        res.json({
            messgage : "Welcome to Movie Booking Application"
        })
    })
    app.post('/movies' ,  async (req ,res) => {
        console.log(req.body);
        let response = await Movie.create(req.body);
        return res.status(201).json({
            messgage : response
        })
    })

    app.listen(PORT , async ()=>{
        console.log(`Server strated at port ${PORT}`);
        connect();
        console.log("mongodb connected");

    })
    
}

SetupServer();