import mongoose from "mongoose";
import { DB_URL } from "./config.js";

export const connect = () => {
  try {
    mongoose.connect(DB_URL);
  } catch (error) {
    console.log(error);
  }
};
