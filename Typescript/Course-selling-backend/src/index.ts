import express, { Express, Request, Response } from "express";
import connect from "./config/db";

const app: Express = express();
const port: number = 3000;

app.get("/", (req: Request, res: Response) => {
  res.send("Hello World! avi");
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
