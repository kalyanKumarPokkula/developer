import express from "express";
import bodyParser from "body-parser";
import { PORT } from "./config/config.js";

import { connect } from "./config/db-config.js";

import API from "./routes/index.js";

const setUp_Server = async () => {
  const app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.use("/api", API);

  app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
    connect();
    console.log("db connected");
  });
};

setUp_Server();
