import "./Nav.css";
import { useNavigate } from "react-router-dom";

const Nav = () => {
  const navigator = useNavigate();
  return (
    <div className="nav">
      <div>
        <h3 onClick={() => navigator("/")}>Goals</h3>
      </div>
      <div>
        <button className="button" onClick={() => navigator("/login")}>
          Log In
        </button>
      </div>
    </div>
  );
};

export default Nav;
