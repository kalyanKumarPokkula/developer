import { NavLink } from "react-router-dom";
import Button from "../UI/Button/Button";
import "./Register.css";

const Login = () => {
  return (
    <form>
      <div className="title">
        <label>Log in</label>
      </div>
      <div className="field">
        <label>Email</label>
        <input type="text" />
      </div>
      <div className="field">
        <label>Password</label>
        <input type="text" />
      </div>
      <Button>Sign up</Button>
      <div className="bottom">
        <p>New here?</p>
        <NavLink to="/register">Register</NavLink>
      </div>
    </form>
  );
};

export default Login;
