import mongoose from "mongoose";

const MovieSchema = new mongoose.Schema({

    name : {
        type : String,
        required : true
    },

    description : {
        type : String,
        required : true
    },

    duration : {
        type : String,
        required : true
    },

    director : {
        type : String,
        required : true
    },

    casts : {
        type : [String],
        required : true
    },

    trailerUrl : {
        type : String,
        required : true
    },

    posterUrl : {
        type : String,
        required : true
    },

    languages : {
        type : [String],
        required :true

    },

    releaseDate : {
        type : String,
        required : true
    },

    releaseStatus : {
        type : String,
        required : true,
        default : 'NotReleased'
        // other options will be released blocked
    }




}, {timestamps : true});

MovieSchema.pre('save' , function(next) {
    let NewArr = this.languages[0].split(',');
    this.languages = NewArr;Ameerpe
    next();
})


const Movie = mongoose.model('Movie' , MovieSchema);

export default Movie;