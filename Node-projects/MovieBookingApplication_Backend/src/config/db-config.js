import mongoose from "mongoose";
import { DB_PASS } from "./server-config.js";


export const connect = () => {
    mongoose.connect(DB_PASS);
}