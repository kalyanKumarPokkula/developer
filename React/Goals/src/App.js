import React, { useState } from "react";

import { Route, Routes } from "react-router-dom";
import CourseGoalList from "./components/CourseGoals/CourseGoalList/CourseGoalList";
import CourseInput from "./components/CourseGoals/CourseInput/CourseInput";
import Nav from "./components/Navbar/Nav";
import Register from "./components/Authentication/Register";
import "./App.css";
import Login from "./components/Authentication/Login";

const App = () => {
  const [courseGoals, setCourseGoals] = useState([
    { text: "Do all exercises!", id: "g1" },
    { text: "Finish the course!", id: "g2" },
  ]);

  const addGoalHandler = enteredText => {
    setCourseGoals(prevGoals => {
      const updatedGoals = [...prevGoals];
      updatedGoals.unshift({ text: enteredText, id: Math.random().toString() });
      return updatedGoals;
    });
  };

  const deleteItemHandler = goalId => {
    setCourseGoals(prevGoals => {
      const updatedGoals = prevGoals.filter(goal => goal.id !== goalId);
      return updatedGoals;
    });
  };

  let content = (
    <p style={{ textAlign: "center" }}>No goals found. Maybe add one?</p>
  );

  if (courseGoals.length > 0) {
    content = (
      <CourseGoalList items={courseGoals} onDeleteItem={deleteItemHandler} />
    );
  }

  return (
    <div>
      <Nav />
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <section className="form_layout">
                <CourseInput onAddGoal={addGoalHandler} />
              </section>

              <section id="goals">
                {content}
                {/* {
                    courseGoals.length > 0 && (
                      <CourseGoalList
                        items={courseGoals}
                        onDeleteItem={deleteItemHandler}
                      />
                    ) // <p style={{ textAlign: 'center' }}>No goals found. Maybe add one?</p>
                  } */}
              </section>
            </div>
          }
        />
        <Route
          path="/register"
          element={
            <section className="form_layout">
              <Register />
            </section>
          }
        />
        <Route
          path="/login"
          element={
            <section className="form_layout">
              <Login />
            </section>
          }
        />
      </Routes>
    </div>
  );
};

export default App;
